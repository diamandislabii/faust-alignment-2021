# Slide Alignment

The following assumes we are attempting to align an IHC (I<sub>2</sub>) to 
a H&E (I<sub>1</sub>) with potential slide-bridges (I<sup>'</sup><sub>1</sub>).

1. In order to evaluate whether the images are eligible for alignment, 
the first part will generate a csv of scores consisting of all 
pairs (I<sub>1</sub>, I<sub>2</sub>) whose homography was able 
to be estimated.

2. The second part performs "slide bridging". For a 
given H&E, we observe all the IHC stains that passed or 
failed to meet the alignment score threshold. Those that failed 
will attempt to be aligned with the ones that succeeded (I<sup>'</sup><sub>1</sub>). 
This will generate a csv consisting of all original & re-matched 
pairs that meet the alignment score threshold 
(I<sub>1</sub>, I<sub>2</sub>) + (I<sup>'</sup><sub>1</sub>, I<sub>2</sub>)

3. The third part creates the final coordinates to use for the IHC. 
In this case, we pass in a folder of lesional coordinates corresponding 
to the H&E. IHC's that failed to be matched or re-matched will not 
have anything generated.

CNN models can be found at https://tinyurl.com/faust-alignment

**NOTE:**
The format of names used is: H&E as name.svs and IHC as name_stain.svs

**Part of "Faust et al. Integrating morphologic and molecular histopathological features through whole slide image registration and deep learning"**