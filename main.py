import glob, os, pathlib
import pandas as pd
from utils.slide import Slide
from utils import sift_matching

if __name__ == '__main__':

    slide_dir = '/path/'
    save_dir = '/path/'

    lesional_coors_folder = '/path/'

    STEP = 1

    if STEP <= 1:
        files = glob.glob(os.path.join(slide_dir, '*.svs'))

        pairs = []
        for f in files:
            if '_' not in pathlib.Path(f).stem:
                for x in pathlib.Path(f).parent.glob('*.svs'):
                    if '_' in x.stem:
                        if x.stem.split('_')[0] == pathlib.Path(f).stem:
                            pairs.append((f, str(x)))

        for n1, n2 in pairs:

            if os.path.exists(os.path.join(save_dir, 'df_bin.csv')) and \
                    pathlib.Path(n1).stem in pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))['H&E'].values and \
                    pathlib.Path(n2).stem in pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))[
                'IHC'].values: continue

            s1 = Slide(n1)
            s2 = Slide(n2)

            sift_matching.evaluator(s1, s2, kernel_sizes=[100], save_dir=save_dir)

    elif STEP <= 2:

        df = pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))
        sift_matching.bridge_slides(df, slide_dir=slide_dir, save_dir=os.path.join(save_dir, 'bridge'))

    elif STEP <= 3:
        df = pd.read_csv(os.path.join(save_dir, 'bridge', 'df_bridged.csv'))
        sift_matching.do_the_transforming(df, slide_dir, lesional_coors_folder,
                                          save_dir=os.path.join(save_dir, 'bridge', 'ihc coors'), div_fac=100)
