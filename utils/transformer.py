import numpy as np
import cv2

def transform_coors(coors, M, div_fac):
    '''
    Transforms the given coordinates

    :param coors: list of coordinates in the form (x, y, x+tilesize, y+tilesize) where (x=0,y=0) is top left
    :param M: homography matrix
    :param div_fac: how much we scaled down the original images
    :return:
    '''

    # NOTE: we are doing the middle of the tile since if we take a corner, rotation will mess things up.
    tile_size = (coors[:, 2] - coors[:, 0])[0]
    midx, midy = (coors[:, 0] + coors[:, 2]) // 2, (coors[:, 1] + coors[:, 3]) // 2
    mid_coors = np.stack((midx, midy), axis=1)
    mid_coors = mid_coors.reshape(-1, 1, 2)

    # homography matrix is between thumbnails. scale it to work on originals
    S = np.identity(3)
    S[0, 0] = div_fac
    S[1, 1] = div_fac
    M = S @ M @ np.linalg.inv(S)

    transformed_coors = cv2.perspectiveTransform(mid_coors.astype(float), M)

    transformed_coors = np.rint(transformed_coors).astype(int)
    transformed_coors = transformed_coors.reshape(-1, 2)
    transformed_coors = transformed_coors.tolist()
    # NOTE: these transformed coordinates are the middle of tile. change back to top left & bot right.
    transformed_coors = [
        (x - tile_size // 2,
         y - tile_size // 2,
         x + int(np.ceil(tile_size / 2)),
         y + int(np.ceil(tile_size / 2)))
        for (x, y) in transformed_coors
    ]

    return transformed_coors