import os
import time
import pathlib
import cv2
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from skimage.exposure.histogram_matching import match_histograms

from utils.tileextractor import TileExtractor
from utils.slide import Slide
from . import transformer


def get_homography(s1, s2, method='sift', min_match_count=4, div_fac=100):
    thumb1 = s1.get_thumbnail((s1.width // div_fac, s1.height // div_fac))
    thumb2 = s2.get_thumbnail((s2.width // div_fac, s2.height // div_fac))

    img1 = np.array(thumb1.convert('L'))
    img2 = np.array(thumb2.convert('L'))

    img2_orig = img2.copy()

    # match the intensity histogram of img2 to img1
    img2 = match_histograms(img2, img1).astype(np.uint8)

    # plotting pixel intensity distributions
    temp = pd.DataFrame({'Pixel Intensity': np.concatenate([img1.flatten(), img2_orig.flatten(), img2.flatten()])})
    temp['Type'] = ['H&E'] * len(img1.flatten()) + ['IHC Raw'] * len(img2_orig.flatten()) + [
        'IHC Post-Normalization'] * len(img2.flatten())
    plt.close('all')
    plt.title('Pixel Intensity Distribution')
    sns.displot(temp, x='Pixel Intensity', col='Type', binwidth=3)
    plt.show()

    start = time.time()

    # Initiate SIFT detector
    if method == 'sift':
        det = cv2.SIFT_create()
    else:
        det = cv2.ORB_create()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = det.detectAndCompute(img1, None)
    kp2, des2 = det.detectAndCompute(img2, None)

    if method == 'sift':
        FLANN_INDEX_KDTREE = 1
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
        search_params = dict(checks=500)  # or pass empty dictionary
    else:
        FLANN_INDEX_LSH = 6
        index_params = dict(algorithm=FLANN_INDEX_LSH,
                            table_number=6,  # 12
                            key_size=12,  # 20
                            multi_probe_level=1)  # 2
        search_params = dict(checks=50)  # or pass empty dictionary

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1, des2, k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m, n in matches:
        if m.distance < 0.7 * n.distance:
            good.append(m)
    print('{} | {} | Good keypoint/descriptor matches: {}'.format(s1.name, s2.name, len(good)))

    if len(good) >= min_match_count:
        src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
        dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

        if M is None: raise Exception('Could not create homography')

        matchesMask = mask.ravel().tolist()

    else:
        raise Exception("Not enough matches are found - {}/{}".format(len(good), min_match_count))

    tot_time = time.time() - start

    draw_params = dict(matchColor=(255, 0, 0),  # draw matches in green color
                       singlePointColor=None,
                       matchesMask=matchesMask,  # draw only inliers
                       flags=2)

    img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, **draw_params)

    plt.close('all')
    plt.axis('off')
    plt.imshow(img3, 'gray')
    plt.title(f'Good matches: {len(good)}. {s1.name} | {s2.name}')
    plt.show()

    return M, {
        'thumb1': thumb1, 'thumb2': thumb2, 'normalized_img1': img1, 'normalized_img2': img2, 'match_count': len(good),
        'tot_time': tot_time
    }


def evaluator(s1, s2, kernel_sizes=(100,), save_dir=None):
    try:
        M, misc = get_homography(s1, s2, min_match_count=4)
    except Exception as e:
        print(e)
        return

    for kernel_size in kernel_sizes:
        # NOTE: modify kernel size in accordance with mpp
        desired_tile_mpp = 0.5040
        factor = desired_tile_mpp / round(s1.mpp, 3) if s1.mpp else 1
        kernel_size = int(kernel_size * factor)
        # the 2 slides could have different mpp's. make equal if they are close enough
        if np.abs(s1.mpp - s2.mpp) < 0.02: s2.mpp = s1.mpp

        # make coordinates. create small patches to use for evaluation
        x = y = 0
        possible_coordinates = []
        while y + kernel_size < s1.height:

            possible_coordinates.append((x, y, x + kernel_size, y + kernel_size))

            # move onto next spot
            x += kernel_size
            if x + kernel_size >= s1.width:
                x = 0
                y += kernel_size

        transformed_coors = transformer.transform_coors(np.array(possible_coordinates), M, div_fac=100)

        te1 = TileExtractor(s1, tile_size=kernel_size)
        te1gen = te1.extract_tiles_coordinates(possible_coordinates, min_non_blank_amt=0, batch_size=1, print_time=True)
        te2 = TileExtractor(s2, tile_size=kernel_size)
        te2gen = te2.extract_tiles_coordinates(transformed_coors, min_non_blank_amt=0, batch_size=1)

        scores = []

        start = time.time()

        while True:
            try:
                res1 = next(te1gen)
                res2 = next(te2gen)
            except StopIteration:
                # done iterating
                break

            # we know all coordinates in first slide exist (since we created all possible kernels).
            # so only the second slide coordinates may not exist (and we iterate to the next possible tile).
            # in that case, move the first slide coordinates forward as well
            for _ in range(len(te2.used_coordinates_mask) - len(te1.used_coordinates_mask)):
                res1 = next(te1gen)

            tile1 = res1['tiles'][0]
            tile2 = res2['tiles'][0]

            amt = 0.1
            istissue1 = TileExtractor.amount_blank(tile1) <= (1 - amt)
            istissue2 = TileExtractor.amount_blank(tile2) <= (1 - amt)

            scores.append(1 - np.abs(bool(istissue1) - bool(istissue2)))

        m, s = divmod(time.time() - start, 60)
        h, m = divmod(m, 60)
        tot_runtime = "{:02d}:{:02d}:{:02d}".format(int(h), int(m), int(s))

        plt.close('all')
        sns.displot(scores)
        plt.title('Accuracy {:.2f}% ({} patches)\nRuntime = {}'.format(np.mean(scores) * 100,
                                                                       len(possible_coordinates), tot_runtime))
        plt.show()

        row = {
            'H&E': s1.name,
            'IHC': s2.name,
            'Evaluator runtime': tot_runtime,
            'Patch size': kernel_size,
            'Mean Score': '{:.2f}'.format(np.mean(scores) * 100),
            'Num good matches': misc['match_count']
        }

        if not os.path.exists(os.path.join(save_dir, 'df_bin.csv')):
            df_bin = pd.DataFrame(
                columns=['H&E', 'IHC', 'Evaluator runtime', 'Patch size', 'Mean Score', 'Num good matches'])
            df_bin.to_csv(os.path.join(save_dir, 'df_bin.csv'), index=False)
        else:
            print('Adding to pre-existing dataframe')

        df_bin = pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))
        df_bin.append(row, ignore_index=True).to_csv(os.path.join(save_dir, 'df_bin.csv'), index=False)


def bridge_slides(df, slide_dir, save_dir, threshold=0.714):
    # NOTE: threshold was derived from 100 ihc stain experiment

    pathlib.Path(save_dir).mkdir(exist_ok=True, parents=True)

    df_good = df[df['Mean Score'] >= (threshold * 100)]
    df_bad = df[df['Mean Score'] < (threshold * 100)]

    for _, row in df_bad.iterrows():
        # check if theres a stain for this H&E that was a good
        for good_ihc in df_good[df_good['H&E'] == row['H&E']]['IHC'].values:

            if os.path.exists(os.path.join(save_dir, 'df_bin.csv')) and \
                    good_ihc in pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))['H&E'].values and \
                    row['IHC'] in pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))['IHC'].values: continue

            # found. evaluate against it
            s1 = Slide(os.path.join(slide_dir, good_ihc + '.svs'))
            s2 = Slide(os.path.join(slide_dir, row['IHC'] + '.svs'))

            evaluator(s1, s2, kernel_sizes=[100], save_dir=save_dir)

    if not os.path.exists(os.path.join(save_dir, 'df_bin.csv')):
        # no bridging was performed
        newdf = df_good
        print('NO BRIDGING PERFORMED')
    else:
        df_bin = pd.read_csv(os.path.join(save_dir, 'df_bin.csv'))

        # group by the bad stains and get its good stain which had the highest score
        df_good2 = df_bin.loc[df_bin.groupby('IHC')['Mean Score'].idxmax()]
        # again filter by threshold
        df_good2 = df_good2[df_good2['Mean Score'] >= (threshold * 100)]

        # contains all the initial good stains + bad stains which were able to be bridged to a good stain
        newdf = df_good.append(df_good2)

    newdf.to_csv(os.path.join(save_dir, 'df_bridged.csv'), index=False)


def do_the_transforming(df, slide_dir, lesional_coors_folder, save_dir, div_fac=100):
    pathlib.Path(save_dir).mkdir(exist_ok=True, parents=True)

    for _, row in df.iterrows():
        s1 = Slide(os.path.join(slide_dir, row['H&E'] + '.svs'))
        s2 = Slide(os.path.join(slide_dir, row['IHC'] + '.svs'))

        # s1 and s2 is guaranteed to have homography
        M, misc = get_homography(s1, s2, min_match_count=4)

        if '_' in s1.name:
            # this is when s2 matched against a good ihc stain. this will exist since the h&e|goodihc pairs
            # are at the top of the dataframe
            coors = np.load(os.path.join(save_dir, '{}_coors.npy'.format(s1.name)))
        else:
            coors = np.load(os.path.join(lesional_coors_folder, '{}_lesion_coors.npy'.format(s1.name)))

        transformed_coors = transformer.transform_coors(coors, M, div_fac)

        np.save(os.path.join(save_dir, '{}_coors.npy'.format(s2.name)), transformed_coors)

